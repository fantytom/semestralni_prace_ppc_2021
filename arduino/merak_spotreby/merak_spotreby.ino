#include <Arduino.h>
#include "EnergyMeter.h"

#define METER_PIN 2
#define PULSES_PER_KILOWATT_HOUR 1000
#define BAUDRATE 9600

EnergyMeter meter(METER_PIN, PULSES_PER_KILOWATT_HOUR);
int S_in;
int i = 0;

void energyConsumed(float energy)
{
  Serial.println(String(energy, 3));   
}

void pulseDetected()
{
  meter.read();
}

void(* resetFunc) (void) = 0;

void setup() {
  meter.onConsumedEnergy(0.001, energyConsumed);
  Serial.begin(BAUDRATE);
  if (!meter.enableInterrupt(pulseDetected))
  {
    Serial.println("209");
    while (true);
  }
  Serial.println(200);
}

void loop() {
  S_in=Serial.readStringUntil('\n').toInt();
  
  
  if(S_in==101)
  {
    Serial.println(201);
  }
  else if(S_in==102)
  {
    Serial.println(202);
    delay(1500);
    while(1)
    {
      meter.update();
      S_in=Serial.readStringUntil('\n').toInt();;
      if(S_in==103)
      {
        Serial.println(203);
        break;
      }
      else if(S_in==108)
      {
        Serial.println(208);
      }
      else if(S_in==101)
      {
        Serial.println(201);
      }
    }
  }
  else if(S_in==104)
  {
    Serial.println(204);
    delay(200);
    resetFunc();
  }
  else if(S_in==108)
  {
    Serial.println(208);
  }
  
}
