#include "merak.h"
#include "ui_merak.h"
#include "aboat.h"

#include <tchar.h>
#include <string>
#include <vector>
#include <assert.h>

#include <QDebug>

double exitValue = 0;
int timerfired = 0;
double value20firesago = 0;
double timestamp20firesago = 0;
int firstrun = 0;
int ConnectionStatus = 0;
int lastRange = 0;

double Serial_run ();
double Serial_Read ();
void Serial_Write (int);

Serial* SP = new Serial("\\\\.\\COM5"); // SERIOVY PORT

merak::merak(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::merak)
{
    ui->setupUi(this);

    ui->comboBox->addItem("Since beginning");
    ui->comboBox->addItem("Since last month");
    ui->comboBox->addItem("Since last day");
    ui->comboBox->addItem("Since last hour");
    ui->comboBox->addItem("Since last 30 minutes");
    ui->comboBox->addItem("Since last 5 minutes");

    if (SP->IsConnected())
    {
        qDebug() << "Connected";
        ui->status_text->setText("error");

        //timer
        timer = new QTimer(this);
        connect(timer,SIGNAL(timeout()),this,SLOT(Serial_timer()));
        timer->start(500);

        //database crate and load
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("namerene_hodnoty.db");
        if (!db.open())
        {
            qDebug() << db.lastError().text();
        }
        QSqlQuery dotaz;
        dotaz.exec("CREATE TABLE IF NOT EXISTS meter (id INTEGER PRIMARY KEY AUTOINCREMENT, consumed REAL, consumption REAL, time INTEGER);");
        dotaz.exec("SELECT consumed, time, consumption FROM meter");
        while (dotaz.next())
        {
            cas.push_back (dotaz.value(1).toDouble());
            hodnota.push_back (dotaz.value(0).toDouble());
            hodnota2.push_back (dotaz.value(2).toDouble());
        }

        //graf begin
        ui->graf->addGraph();
        ui->graf->graph(0)->setPen(QPen(Qt::blue));
        ui->graf->graph(0)->setData(cas, hodnota);
        QSharedPointer <QCPAxisTickerDateTime> dateTick (new QCPAxisTickerDateTime);
        dateTick->setDateTimeFormat("dd.MM.yy\nhh:mm:ss");
        ui->graf->xAxis->setTicker(dateTick);
        ui->graf->graph(0)->rescaleAxes();
        ui->graf->xAxis->setLabel("Time");
        ui->graf->yAxis->setLabel("kWh");
        ui->graf->xAxis2->setVisible(true);
        ui->graf->yAxis2->setVisible(true);
        ui->graf->xAxis2->setTicks(false);
        ui->graf->yAxis2->setTicks(false);
        ui->graf->xAxis2->setTickLabels(false);
        ui->graf->yAxis2->setTickLabels(false);
        //ui->graf->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc));
        ui->graf->replot();

        ui->graf_2->addGraph();
        ui->graf_2->graph(0)->setPen(QPen(Qt::red));
        ui->graf_2->graph(0)->setData(cas, hodnota2);
        ui->graf_2->xAxis->setTicker(dateTick);
        ui->graf_2->graph(0)->rescaleAxes();
        ui->graf_2->xAxis->setLabel("Time");
        ui->graf_2->yAxis->setLabel("W");
        ui->graf_2->xAxis2->setVisible(true);
        ui->graf_2->yAxis2->setVisible(true);
        ui->graf_2->xAxis2->setTicks(false);
        ui->graf_2->yAxis2->setTicks(false);
        ui->graf_2->xAxis2->setTickLabels(false);
        ui->graf_2->yAxis2->setTickLabels(false);
        //ui->graf_2->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc));
        ui->graf_2->replot();


    }
    else
    {
        ui->status_text->setText("NOT connected! \nPlease restart program");
        QMessageBox::critical(this, "Connection Error", "Arduino NOT connected! Application needs to be closed.");
        ui->centralwidget->setEnabled(false);
        ui->menubar->setEnabled(false);
        //QApplication::quit();
    }
}

merak::~merak()
{
    delete ui;
}

void merak::Serial_timer()
{
    // get time
    unsigned long timestamp = QDateTime::currentMSecsSinceEpoch()/1000;

    QDateTime time = QDateTime::currentDateTime();
    QString time_text = time.toString("hh:mm:ss dd.MM.yyyy");
    ui->time_label->setText("Time: " + time_text);

    // controlni a pocatecni hodnoty
    if(exitValue==0 && !hodnota.empty())
    {
        exitValue = hodnota.back();
        timestamp20firesago = timestamp;
        ui->display_total->setText(QString::number(exitValue)+" kWh");
        ui->display_total_range->setText(QString::number(exitValue)+" kWh");
    }
    if(exitValue==0 && hodnota.empty())
    {
        ui->display_total->setText(QString::number(exitValue)+" kWh");
        ui->display_total_range->setText(QString::number(exitValue)+" kWh");
    }
    else if(timerfired == 0)
    {
        timestamp20firesago = timestamp;
    }
    timerfired++;

    // cteni ze Serialu
    double serialData;
    double value_curr;

    serialData = Serial_Read();
    if(serialData !=0)
        {
            qDebug() << serialData;

            if(serialData == 200) // Online.
                {ui->status_text->setText("Online.");value_curr=0;}
            else if(serialData == 201) // Ping.
                {ui->status_text->setText("Ping :) .");value_curr=0;}
            else if(serialData == 202) // Start
            {
                ui->status_text->setText("Reading.");

                if(firstrun==0) // vyjimka pro prazdnou DB
                {
                    hodnota.push_back(exitValue);
                    hodnota2.push_back(0);
                    cas.push_back(timestamp);

                    QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(exitValue).arg(0).arg(timestamp);
                    //qDebug() << sql;
                    QSqlQuery query(sql);
                }
                else
                {
                    hodnota.push_back(hodnota.back());
                    hodnota2.push_back(0);
                    cas.push_back(timestamp);

                    QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(hodnota.back()).arg(0).arg(timestamp);
                    //qDebug() << sql;
                    QSqlQuery query(sql);
                }

                value_curr=0;
            }
            else if(serialData == 203) // Stop.
            {
                ui->status_text->setText("Stop.");
                ui->display_now->setText("0 W");

                hodnota.push_back(hodnota.back());
                hodnota2.push_back(0);
                cas.push_back(timestamp);

                QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(hodnota.back()).arg(0).arg(timestamp);
                //qDebug() << sql;
                QSqlQuery query(sql);

                value_curr=0;
            }
            else if(serialData == 204) // Reset
            {
                ui->status_text->setText("Reseting.");
                Sleep(3000);
                ui->Reset_Button->setEnabled(true);
                value_curr=0;
            }
            else if(serialData == 209) // Pin Error
                {ui->status_text->setText("Pin Error.");value_curr=0;}
            else if(serialData == 208) // Connection check answer
                {ConnectionStatus = 0; value_curr=0;}
            else // prisla nejaka data
            {
                value_curr =  serialData+exitValue;
            }
        }
        else
        {
            value_curr = 0;
        }

        if(value_curr !=0)
        {
            //graf update
            hodnota.push_back(value_curr);
            cas.push_back(timestamp);
            ui->graf->graph(0)->setData(cas, hodnota);
            ui->graf->graph(0)->rescaleAxes();
            ui->graf->replot();

            ui->display_total->setText(QString::number(value_curr)+" kWh"); // label total vypis

            //calculation
            double momentalni = 0;
            if(timerfired>20) //kazdych 20 pulvterin
            {
                double namereno;
                double interval;

                if (firstrun==0)
                {
                    namereno = 0;
                    interval = (timestamp - timestamp20firesago)/3600;
                    firstrun = 1;
                }
                else
                {
                    namereno = value_curr - value20firesago;
                    interval = (timestamp - timestamp20firesago)/3600;
                }

                momentalni = (1000*namereno)/interval;
                qDebug() << momentalni;

                timerfired = 0;
                value20firesago = hodnota.back();
                timestamp20firesago = timestamp;
            }

            //graf2 update
            if(momentalni == 0 && !hodnota2.empty())
                hodnota2.push_back(hodnota2.back());
            else
                hodnota2.push_back(momentalni);
            ui->graf_2->graph(0)->setData(cas, hodnota2);
            ui->graf_2->graph(0)->rescaleAxes();
            ui->graf_2->replot();

            int display_now_number = hodnota2.back();
            ui->display_now->setText(QString::number(display_now_number)+" W"); // label now vypis

            QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(serialData+exitValue).arg(hodnota2.back()).arg(timestamp);            
            QSqlQuery query(sql);

            //consumed in range vypis
            double inRange;
            inRange = hodnota.back()-hodnota.front();
            ui->display_total_range->setText(QString::number(inRange)+" kWh");

        }

    merak::ConnectionCheck(); // conncection status check
}

void merak::Range_change(unsigned long timestamp)
{
    if(ui->comboBox->currentIndex() == 0)
    {
        cas.clear();
        hodnota.clear();
        hodnota2.clear();

        QSqlQuery dotaz;
        dotaz.exec("SELECT consumed, time, consumption FROM meter");
        while (dotaz.next())
        {
            cas.push_back (dotaz.value(1).toDouble());
            hodnota.push_back (dotaz.value(0).toDouble());
            hodnota2.push_back (dotaz.value(2).toDouble());
        }

        ui->graf->graph(0)->setData(cas, hodnota);
        ui->graf->graph(0)->rescaleAxes();
        ui->graf->replot();

        ui->graf_2->graph(0)->setData(cas, hodnota2);
        ui->graf_2->graph(0)->rescaleAxes();
        ui->graf_2->replot();
    }

    else if(ui->comboBox->currentIndex() == 1) // 2592000 v mesici
    {
        Range_update(timestamp,2592000);
    }
    else if(ui->comboBox->currentIndex() == 2) // 86400 ve dni
    {
        Range_update(timestamp,86400);
    }
    else if(ui->comboBox->currentIndex() == 3) // 3600 v hodině
    {
        Range_update(timestamp,3600);
    }
    else if(ui->comboBox->currentIndex() == 4) // 1800 v 30 min
    {
        Range_update(timestamp,1800);
    }
    else if(ui->comboBox->currentIndex() == 5) // 300 v 5 min
    {
        Range_update(timestamp,300);
    }
}

void merak::Range_update(unsigned long timestamp,unsigned long time)
{
    QSqlQuery dotaz;
    QString dotaz_string = QString("SELECT consumed, time, consumption FROM meter WHERE time > %1").arg(timestamp-time);
    dotaz.exec(dotaz_string);

    cas.clear();
    hodnota.clear();
    hodnota2.clear();

    cas.push_back (timestamp-time);
    hodnota.push_back (0);
    hodnota2.push_back (0);

    while (dotaz.next())
    {
        cas.push_back (dotaz.value(1).toDouble());
        hodnota.push_back (dotaz.value(0).toDouble());
        hodnota2.push_back (dotaz.value(2).toDouble());
    }

    if (hodnota.size()<3)
    {
        ui->comboBox->setCurrentIndex(0);
        Range_change(timestamp);
        QMessageBox::warning(this, "Empty", "No Values in selected range.");
    }
    else
    {
        hodnota2[1] = 0;
        hodnota[0] = hodnota[1];

        ui->graf->graph(0)->setData(cas, hodnota);
        ui->graf->graph(0)->rescaleAxes();
        ui->graf->yAxis->setRangeLower(0);
        ui->graf->replot();

        ui->graf_2->graph(0)->setData(cas, hodnota2);
        ui->graf_2->graph(0)->rescaleAxes();
        ui->graf->yAxis->setRangeLower(0);
        ui->graf_2->replot();

        double inRange; //consumed in range vypis
        inRange = hodnota.back()-hodnota.front();
        ui->display_total_range->setText(QString::number(inRange)+" kWh");
    }
}

void merak::ConnectionCheck()
{
    if (ConnectionStatus == 0)
    {
            ConnectionStatus = 0;
    }
    else if (ConnectionStatus == 50)
    {
        Serial_Write(108);
    }
    else if (ConnectionStatus == 70)
    {
        Serial_Write(108);
    }
    else if (ConnectionStatus == 100)
    {
        qDebug() << "not connected";
        ui->status_text->setText("Connection LOST! \nPlease restart program");
        ui->centralwidget->setEnabled(false);
        ui->menubar->setEnabled(false);
        QMessageBox::critical(this, "Connection Error", "Arduino NOT connected! Application needs to be closed.");
        QApplication::quit();
    }
    ConnectionStatus++;
}

double Serial_Read ()
{
    double doubleData = 0;

    char incomingData[256] = "";
    int dataLength = 255;
    int readResult = 0;

    readResult = SP->ReadData(incomingData,dataLength);
    incomingData[readResult] = 0;

    if (incomingData[0] != 0)
        assert(sscanf(incomingData, "%lf", &doubleData) == 1);

    //qDebug() <<  incomingData;

    return doubleData;
}

void Serial_Write (int outgoing)
{
    int dataLength = 255;
    char outgoingData[256] = "";
    //SP->WriteData(outgoingData,dataLength);
    if(outgoing != 0)
    {
        std::sprintf(outgoingData, "%d", outgoing);
        SP->WriteData(outgoingData,dataLength);
        qDebug() << outgoingData;
    }
}

void merak::on_Ready_Button_clicked()
{
    Serial_Write (101);
    ui->status_text->setText("Ping?");
}


void merak::on_Start_Button_clicked()
{
    Serial_Write (102);
    ui->status_text->setText("Start?");

    ui->Start_Button->setEnabled(false);
    ui->Stop_Button->setEnabled(true);
    ui->Reset_Button->setEnabled(false);
    ui->actionStart_measuring->setEnabled(false);
    ui->actionStop_measuring->setEnabled(true);
    ui->actionReset->setEnabled(false);
    ui->actionClear_all_entries->setEnabled(false);

}


void merak::on_Stop_Button_clicked()
{
    Serial_Write (103);
    ui->status_text->setText("Stop?");

    ui->Start_Button->setEnabled(true);
    ui->Stop_Button->setEnabled(false);
    ui->Reset_Button->setEnabled(true);
    ui->actionStart_measuring->setEnabled(true);
    ui->actionStop_measuring->setEnabled(false);
    ui->actionReset->setEnabled(true);
    ui->actionClear_all_entries->setEnabled(true);
}


void merak::on_Reset_Button_clicked()
{
    Serial_Write (104);
    ui->status_text->setText("Reset?");

    ui->Reset_Button->setEnabled(false);

    if(!hodnota.empty())
        exitValue = hodnota.back();
}


void merak::on_actionClear_all_entries_triggered()
{
    QString sql = QString("DELETE FROM meter");
    QSqlQuery query(sql);
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}


void merak::on_actionExit_triggered()
{
    QApplication::quit();
}


void merak::on_actionPing_triggered()
{
    on_Ready_Button_clicked();
}


void merak::on_actionStart_measuring_triggered()
{
    on_Start_Button_clicked();
}


void merak::on_actionStop_measuring_triggered()
{
    on_Stop_Button_clicked();
}


void merak::on_actionReset_triggered()
{
    on_Reset_Button_clicked();
}


void merak::on_actionAboat_triggered()
{
    Aboat aboat;
    aboat.setModal(true);
    aboat.exec();
}

void merak::on_pushButton_clicked()
{
    unsigned long timestamp = QDateTime::currentMSecsSinceEpoch()/1000;
    Range_change(timestamp);
}

void merak::closeEvent (QCloseEvent *event)
{
    unsigned long timestamp = QDateTime::currentMSecsSinceEpoch()/1000;
    if(firstrun==0)
    {
        QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(exitValue).arg(0).arg(timestamp);
        //qDebug() << sql;
        QSqlQuery query(sql);
    }
    else
    {
        QString sql = QString("INSERT INTO meter (consumed,consumption, time) VALUES (%1, %2, %3)").arg(hodnota.back()).arg(0).arg(timestamp);
        //qDebug() << sql;
        QSqlQuery query(sql);
    }
    event->accept();
}

