#include "aboat.h"
#include "ui_aboat.h"

Aboat::Aboat(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Aboat)
{
    ui->setupUi(this);

    ui->git->setText("<a href=\"https://gitlab.fel.cvut.cz/fantytom/semestralni_prace_ppc_2021\">Git repository (in Czech)</a>");
    ui->git->setTextFormat(Qt::RichText);
    ui->git->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->git->setOpenExternalLinks(true);
}

Aboat::~Aboat()
{
    delete ui;
}
