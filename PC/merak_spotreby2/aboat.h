#ifndef ABOAT_H
#define ABOAT_H

#include <QDialog>

namespace Ui {
class Aboat;
}

class Aboat : public QDialog
{
    Q_OBJECT

public:
    explicit Aboat(QWidget *parent = nullptr);
    ~Aboat();

private:
    Ui::Aboat *ui;
};

#endif // ABOAT_H
