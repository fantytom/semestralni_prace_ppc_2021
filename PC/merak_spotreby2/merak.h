#ifndef MERAK_H
#define MERAK_H

#include <QMainWindow>
#include <QTimer>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDateTime>
#include <QMessageBox>
#include <QCloseEvent>

#include "SerialClass.h"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class merak; }
QT_END_NAMESPACE

class merak : public QMainWindow
{
    Q_OBJECT

    QVector<double> cas, hodnota;
    QVector<double> hodnota2;

public:
    merak(QWidget *parent = nullptr);
    ~merak();

    void ConnectionCheck();

public slots:
    void Serial_timer();
    void Range_change(unsigned long);
    void Range_update(unsigned long , unsigned long);

private slots:
    void on_Ready_Button_clicked();

    void on_Start_Button_clicked();

    void on_Stop_Button_clicked();

    void on_Reset_Button_clicked();

    void on_actionClear_all_entries_triggered();

    void on_actionExit_triggered();

    void on_actionPing_triggered();

    void on_actionStart_measuring_triggered();

    void on_actionStop_measuring_triggered();

    void on_actionReset_triggered();

    void on_actionAboat_triggered();

    void on_pushButton_clicked();

    void closeEvent(QCloseEvent *event);
private:
    Ui::merak *ui;
    QTimer *timer;
};
#endif // MERAK_H
