# Semestralni_prace_PPC_2021
Vytvořeno Tomášem Fantyšem v rámci předmětu PPC v letním semestru 2020/2021

Věnuje se vytvoření grafického rozhraní pro měřič spotřeby založený na integrovaném obvodu ADA7755.
Skládá se ze 2 komponent. Zaprvé Arduinu Nano na kterém věží kód starající se o čtení dat a jejich vypisování na seriový výstup. Zadruhé programu vytvořeném v QT creator, který data ze sériového vstupu čte a zpracovává.

# Návod k použití
Potřebujute Arduino Nano (nebo případně jeho čínský klon CH340, který jsem používal já). Jeho Digital Pin 2 připojíte na CF výstup ADE7755 integrovaného čipu, který naleznete ve velké většině měřičů spotřeby. Kde se CF výstup nachází naleznete v této [dokumentaci](https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7755.pdf). Samozřejmě nezapomeňtě propojit i Digital Ground.

Na Arduino nahrajete kód ze složky [arduino/merak_spotreby](https://gitlab.fel.cvut.cz/fantytom/semestralni_prace_ppc_2021/-/tree/main/arduino/merak_spotreby). 
(Je možné celý program otesovat i bez měřiče spotřeby pokud propojíte Digital Pin 2 a Digital Pin 4 a použijete kód ze složky [arduino/merak_spotreby_test](https://gitlab.fel.cvut.cz/fantytom/semestralni_prace_ppc_2021/-/tree/main/arduino/merak_spotreby_test). Arduino si bude signál generovat samo.)

Na sériovém portu se komunikuje pomocí trojciferných příkazů.
| Send | Reply |
| ------ | ------ |
|  | 200 - Online. |
| 101 - Ping | 201 - Ping. |
| 102 - Start measuring | 202 - Reading. |
| 103 - Stop measuring | 203 - Stop. |
| 104 - Reset | 204 - Reset. |
| 108 - Ping2 | 208 - Ping2. |
|  | 209 - Pin error. |

Po předání příkazu Start začne Arduino měřit hodnoty a každou spotřebovanou 0.001 kWh vypíše na sériový výstup.

Avšak mnohem lepší je propojení s programem určeným pro Windows uložený ve složce [PC](https://gitlab.fel.cvut.cz/fantytom/semestralni_prace_ppc_2021/-/tree/main/PC). Než ho zkompilujete (pozor je to QT program!) je třeba v _merak.cpp_ na 24 řádku vyplnit správný sériový port, ke kterému je Vaše Arduino připojené.

Když program otevřete a načte se spojení s Arduinem měli byste vidět v okénku Status hlášku Online. Tlačítka označená jako Arduino Signals fungují přesně jako trojciferné příkazy uvedené výše. Jakmile zmáčknete Ping, odešle se signál 101, status se změní na "Ping?" a pokud Arduino odpoví 201 status se znovu změní tentokrát na "Ping :) ."

Abyste začli sbírat data je třeba kliknout na tlačítko Start measuring a po chvíli začnou data přicházet. Rovnou se budou ukládat do databáze a vykreslovat na graf.

Momentální spotřeba se obnovuje pouze přibližně každých 10 sekund, protože je třeba jí spočítat z příchozích dat.

Nabídka Range umožnuje nastavit kolik dat grafy budou zobrazovat. Je vždy nutné výběr potvrdit fajfkou vpravo.

Dále je možné v nabídce Database vymazat všechny uložené vstupy v databázi (ale pouze pokud se nesbírají data).

Kdyby Vás zajímalo k čemu se používá signál 108 a 208, je to k periodickému dotazu zda je arduino stále připojené a když dlouho neodpoví, program vyhlásí chybu ztráty spojení.

# Postup mojí práce
Začínal jsem nejdřív zkoušením způsobu, jak se připojit k měřiči spotřeby. Toto bylo velice vyčerpávající a trvalo mi to několik týdnů, protože se mi to stále nedařilo. Prošel jsem si přes spostu hledání v dokumentacích a zkoušení různých knihovem (třeba [této](https://github.com/openenergymonitor/EmonLib)). Nakonec jsem však našel tuto [knihovnu](https://github.com/elC0mpa/EnergyMeter), která slibovala jednoduché propojení s [ADE7755](https://www.analog.com/media/en/technical-documentation/data-sheets/ADE7755.pdf). A to se mi povedlo, několik dnů, co jsem pracoval na kódu pro hlavní program, to opravdu fungovalo. Ale poté, z níčeho nic, s pořád stejným kódem, to fungovat přestalo. Pokaždé když jsem Arduino připojil k měřiči, tak zamrzlo a občas se na něj nedal nahrát ani nový kód. Strávil jsem několik dnů troubleshootingem, ale rozfungovat se mi to nepovedlo. Tato knihovna má funkci i samogenerování potřebného signálu a jelikož pracuji na posední chvíli, tuto funci jsem se rozhodl použít pro dokončení programu. Ale určitě se k tomu chci někdy vrátit a opravit to,

Vytváření programu v QT Creatoru bylo podstatně jednodužší a jsem rád že jsem si ho zvolil oproti psaní celého kódu. Kromě klasických QT nástrojů jsem použil ještě QCustomPlot na vykreslování grafů a [Serial knihovnu z Arduno Playground](https://playground.arduino.cc/Interfacing/CPPWindows/) pro připojení k sériovém portu.
Výsledek je možná trochu zmatený, ale jsem s ním spokojený.

# Použité knihovny
[EnergyMeter](https://github.com/elC0mpa/EnergyMeter) by [José Gabriel Companioni Benítez (elC0mpa)](https://github.com/elC0mpa)

[Arduino and C++ (for Windows)](https://playground.arduino.cc/Interfacing/CPPWindows/) by [Arduino Playground](https://playground.arduino.cc/)

[QT framework](https://www.qt.io/)

[QCustomPlot](https://www.qcustomplot.com/)
